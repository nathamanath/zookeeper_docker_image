# Zookeeper on alpine linux

Aim here is to create a minimal zookeeper image which can conform with CIS
docker benchmarks.

## Building

```bash
  make
```

## Usage

### Zookeeper Standalone

* mount config
* mount myuid file

```bash
  docker run \
  -v $(pwd)/configs/zoo.cfg:/opt/zookeeper/conf/zoo.cfg \
  -p 2181:2181 \
  -t nathamanath/zookeeper
```

basic config included in `./configs`

https://zookeeper.apache.org/doc/current/zookeeperStarted.html

### Zookeeper Cluster

docker-compose.yml shows this image used to set up a zookeeper cluster. Required
config is in `./configs`. `docker-compose up` chould get this going.

This requires more configuration, and for you to set `/var/lib/zookeeper/myid`.
This file must containa an integer. it must be different per node.

https://zookeeper.apache.org/doc/current/zookeeperStarted.html#sc_RunningReplicatedZooKeeper

