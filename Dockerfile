FROM anapsix/alpine-java:8_jdk

RUN apk update && apk add \
  wget \
  netcat-openbsd \
  gnupg

RUN rm -rf /var/cache/apk/*

ARG ZOOKEEPER_VERSION=3.4.12

RUN wget -O /tmp/zookeeper.tar.gz http://apache.mirror.anlx.net/zookeeper/zookeeper-${ZOOKEEPER_VERSION}/zookeeper-${ZOOKEEPER_VERSION}.tar.gz
RUN wget -O /tmp/zookeeper.asc https://apache.org/dist/zookeeper/zookeeper-${ZOOKEEPER_VERSION}/zookeeper-${ZOOKEEPER_VERSION}.tar.gz.asc

# Verify zookeeper
RUN wget -q -O - https://apache.org/dist/zookeeper/KEYS | gpg --import
RUN gpg --verify /tmp/zookeeper.asc /tmp/zookeeper.tar.gz

RUN tar -xvzf /tmp/zookeeper.tar.gz -C /tmp
RUN mv /tmp/zookeeper-${ZOOKEEPER_VERSION} /opt/zookeeper

# Tidy up
RUN apk del wget
RUN rm /tmp/zookeeper.tar.gz /tmp/zookeeper.asc

# Healthcheck
COPY docker_healthcheck /usr/local/bin/docker_healthcheck
RUN chmod +x /usr/local/bin/docker_healthcheck
HEALTHCHECK CMD ["docker_healthcheck"]

# Non root user
RUN adduser -D -H -s /bin/bash zookeeperuser
# data dir
RUN mkdir -p /var/lib/zookeeper
# logs dir
RUN mkdir -p /var/log/zookeeper
RUN chown -R zookeeperuser /var/lib/zookeeper
RUN chown -R zookeeperuser /var/log/zookeeper
USER zookeeperuser
WORKDIR /opt/zookeeper

ARG BUILD_DATE
ARG VERSION

LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name = "zookeeper"
LABEL org.label-schema.description = "apache zookeeper version ${ZOOKEEPER_VERSION} installed on alpine linux"
LABEL org.label-schema.vendor = "nathang"
LABEL org.label-schema.version = $VERSION
LABEL org.label-schema.schema-version = "1.0"

EXPOSE 2181

CMD /opt/zookeeper/bin/zkServer.sh start-foreground
