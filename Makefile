IMAGE_NAME = nathamanath/zookeeper_docker_image
VERSION = $(shell cat ./version.txt)
REGISTRY_URL=registry.gitlab.com

docker: docker.build docker.tag docker.push

docker.build:
	docker build \
		--build-arg VERSION=$(VERSION) \
		--build-arg BUILD_DATE="$(shell date --rfc-3339=seconds)" \
		-t $(IMAGE_NAME) \
		.

docker.tag:
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)

docker.push:
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)
